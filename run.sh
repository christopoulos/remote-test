#!/bin/sh

set -e

## Set up the database

mix deps.get

# echo "\nCreating DB..."
# mix ecto.setup

echo "\n Launching Phoenix web server..."
# Start the phoenix web server
mix phx.server