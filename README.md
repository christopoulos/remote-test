# UsersApi

To run the UsersApi service:

  * Build the docker image `docker-compose build`
  * Create and migrate your database with `docker-compose run web mix ecto.setup`
  * Start the service with `docker-compose up`

Alternatively uncomment lines 10-11 on `run.sh` and run `docker-compose up` (Make sure the second time you run it you remove the migration code as it takes too long)

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

To run the tests run `docker-compose run web mix test`

# Api Details

The following Api endpoints are supported

## Get basic response

### Request

`GET /`

    curl -i -H 'Accept: application/json' http://localhost:4000/

### Response

    HTTP/1.1 200 OK
    cache-control: max-age=0, private, must-revalidate
    content-length: 178
    content-type: application/json; charset=utf-8
    date: Sat, 17 Dec 2022 09:14:00 GMT
    server: Cowboy
    x-request-id: FzGJyqYeq9YoHEsAAAAk

    {
      "data":{
        "timestamp":"2022-12-17T09:14:00.655629Z",
        "users":[
          {"id":"e03e5aa6-b692-43f0-99f8-f4ef2143c376","points":45},
          {"id":"1d9f5c41-1593-41ff-bdd4-118dc84188e2","points":69}
        ]
      }
    }

Wrapped the response in a data field for uniformity with the rest of the endpoints and also to comply with https://jsonapi.org/

## Get list of Users

### Request

`GET /users/`

    curl -i -H 'Accept: application/json' http://localhost:4000/users/

### Response

    HTTP/1.1 200 OK
    cache-control: max-age=0, private, must-revalidate
    content-length: 590
    content-type: application/json; charset=utf-8
    date: Sat, 17 Dec 2022 09:19:22 GMT
    server: Cowboy
    x-request-id: FzGKFej0ENSI51IAAAIj

    {
      "data":[
        {"id":"e03e5aa6-b692-43f0-99f8-f4ef2143c376","points":56},
        {"id":"1d9f5c41-1593-41ff-bdd4-118dc84188e2","points":96},
        {"id":"28099406-53cd-4a2e-b2cf-1816782290e1","points":68}
      ]
    }

## Get a specific User

### Request

`GET /users/:id`

    curl -i -H 'Accept: application/json' http://localhost:4000/users/e03e5aa6-b692-43f0-99f8-f4ef2143c376

### Response

    HTTP/1.1 200 OK
    cache-control: max-age=0, private, must-revalidate
    content-length: 66
    content-type: application/json; charset=utf-8
    date: Sat, 17 Dec 2022 09:28:55 GMT
    server: Cowboy
    x-request-id: FzGKmymQKeqAbwAAAAJk

    {"data":{"id":"e03e5aa6-b692-43f0-99f8-f4ef2143c376","points":94}}


## Create a new User

### Request

`POST /users/`

    curl -i -H 'Accept: application/json' -d "points=42"  -X POST http://localhost:4000/users

### Response

    HTTP/1.1 201 Created
    cache-control: max-age=0, private, must-revalidate
    content-length: 66
    content-type: application/json; charset=utf-8
    date: Sun, 18 Dec 2022 12:23:29 GMT
    location: /users/b4b9da04-4038-43c7-b7f0-fa92b22d9582
    server: Cowboy
    x-request-id: FzHitnDPuqYNqfIAAABh

    {"data":{"id":"b4b9da04-4038-43c7-b7f0-fa92b22d9582","points":42}}


## Update a User

### Request

`PUT /users/:id`

    curl -i -H 'Accept: application/json' -d "points=45"  -X PUT http://localhost:4000/users/b4b9da04-4038-43c7-b7f0-fa92b22d9582

### Response

    HTTP/1.1 200 OK
    cache-control: max-age=0, private, must-revalidate
    content-length: 66
    content-type: application/json; charset=utf-8
    date: Sun, 18 Dec 2022 13:22:42 GMT
    server: Cowboy
    x-request-id: FzHl1N275qTpNS8AAASD

    {"data":{"id":"b4b9da04-4038-43c7-b7f0-fa92b22d9582","points":45}}


## Delete a User

### Request

`DELETE /users/:id`

    curl -i -H 'Accept: application/json' -X DELETE http://localhost:4000/users/b4b9da04-4038-43c7-b7f0-fa92b22d9582

### Response

    HTTP/1.1 204 No Content
    cache-control: max-age=0, private, must-revalidate
    date: Sun, 18 Dec 2022 13:28:16 GMT
    server: Cowboy
    x-request-id: FzHmI5FEhfs_LggAAAIE


# Implementation assumptions
In order to implement this solution the following assumtions were made:
  * If it was to be run on a production system a vault service would be used to store db credentials, and then set in `prod.exc`. Now as we run it locally we can have simple credentials setup in `docker-compose.yml`
  * Also if it was to be run on prod I would expect the db to run on a separate system.
  * As we were handling some db entities it made sense to me to keep the autocreated rest endpoints to give the ability to an external user to handle the existing data. Having said that I would add token validation in the requests and data validation if it was to be properly used

# Possible improvements
In here we can lsit possible improvements to the project
  * Add more tests cases (both unit and integrations tests).
  * Add pagination to `GET /users` endpoint
  * In general deleting data is a bad practice, I would add a soft delete functionality
