# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     UsersApi.Repo.insert!(%UsersApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

  alias UsersApi.Repo
  alias UsersApi.Users.User

  result = for i <- 0..1000000, do: Repo.insert! %User{
    points: 0
  }

