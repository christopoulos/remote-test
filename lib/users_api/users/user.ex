defmodule UsersApi.Users.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :points, :integer

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:points])
    |> validate_required([:points])
    |> validate_points_range(:points)
  end

  defp validate_points_range(changeset, field) do 
    value = get_field(changeset, field)
    if 0 <= value and value <= 100 do
      changeset
    else
      add_error(changeset, field, "Points out of range 0-100")
    end
  end
end
