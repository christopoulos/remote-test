defmodule ApiState do
  use GenServer
  @callback view() :: tuple()

  defmodule StateStruct do
    defstruct timestamp: nil, min_number: 0
  end

  # Client
  def start_link() do
    GenServer.start_link(__MODULE__, struct(StateStruct))
  end

  def view() do
    GenServer.call(__MODULE__, {':view'})
  end

  # server
  @impl true
  def init(args) do
    schedule_work()
    {:ok, args}
  end

  @impl true
  def handle_call({':view'}, _from, state) do
    state = %StateStruct{timestamp: DateTime.utc_now(), min_number: state.min_number}

    {:reply, state, state}
  end


  @impl true
  def handle_info(:work, state) do
    # Do the desired work here

    alias UsersApi.Repo
    alias UsersApi.Users.User
    import Ecto.Query
    
    update(User, set: [points: fragment("floor(random()*100)")])
    |> Repo.update_all([])
    
    state = %StateStruct{timestamp: state.timestamp, min_number: :rand.uniform(100)}

    
    # Loop again and reschedule
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work do
    # After 60 seconds(60 * 1000 in milliseconds) the desired task will take place.
    Process.send_after(self(), :work , 60*1000)
  end
end