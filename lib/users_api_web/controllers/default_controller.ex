defmodule UsersApiWeb.DefaultController do
  use UsersApiWeb, :controller
  alias UsersApi.Users

  def index(conn, _params) do
    state = api_state().view()

    json_response = response_json(state)
    json conn, json_response
  end

  defp api_state do
    Application.get_env(:users_api, :api_state)
  end

  defp response_json(state) do
    query_res = Users.get_users_over_points(state.min_number)

    %{
      data: %{
        users: Enum.map(query_res, fn x -> %{id: x.id, points: x.points} end),
        timestamp: state.timestamp
      }
    }
    

  end
end