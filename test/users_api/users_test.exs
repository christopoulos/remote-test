defmodule UsersApi.UsersTest do
  use UsersApi.DataCase

  alias UsersApi.Users

  describe "users" do
    alias UsersApi.Users.User

    import UsersApi.UsersFixtures

    @invalid_attrs %{points: nil}
    @invalid_attrs2 %{points: 120}

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Users.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Users.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      valid_attrs = %{points: 42}

      assert {:ok, %User{} = user} = Users.create_user(valid_attrs)
      assert user.points == 42
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      update_attrs = %{points: 43}

      assert {:ok, %User{} = user} = Users.update_user(user, update_attrs)
      assert user.points == 43
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Users.update_user(user, @invalid_attrs)
      assert user == Users.get_user!(user.id)
    end

    test "update_user/2 with invalid data returns error changeset points not in 0-100" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Users.update_user(user, @invalid_attrs2)
      assert user == Users.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Users.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Users.change_user(user)
    end


    test "get_users_over_points/1 returns 2 users over min poins" do
      user_fixture(%{points: 38})
      user_fixture(%{points: 39})
      user_fixture(%{points: 40})
      user_fixture(%{points: 40})
      user_fixture(%{points: 40})

      users = Users.get_users_over_points(39)
      assert length(users) == 2
      for user <- users do
        assert user.points == 40
      end
    end
  end
end
