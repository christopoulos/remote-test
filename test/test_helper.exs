ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(UsersApi.Repo, :manual)

Mox.defmock(ApiState.BaseMock, for: ApiState)
Application.put_env(:users_api, :api_state, ApiState.BaseMock)
