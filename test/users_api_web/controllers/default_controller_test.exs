defmodule UsersApiWeb.DefaultControllerTest do
  use UsersApiWeb.ConnCase
  use ExUnit.Case, async: true
  import Mox

  import UsersApi.UsersFixtures
  import UsersApiWeb.DefaultController
  alias UsersApi.Users.User
  use ExUnit.Case, async: true

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup [:create_users]
    test "get default response", %{conn: conn} do
      ApiState.BaseMock
      |> expect(
        :view, fn() -> %ApiState.StateStruct{timestamp: ~U[2022-12-15 22:06:11.409030Z], min_number: 41} end
      )
      expected_response = %{
          "timestamp" => "2022-12-15T22:06:11.409030Z", 
          "users" => [
              %{"id" => "", "points" => 42},
              %{"id" => "", "points" => 43}
          ]
      }
      conn = get(conn, Routes.default_path(conn, :index))
      assert json_response(conn, 200)["data"]["timestamp"] == expected_response["timestamp"]
      assert Enum.at(json_response(conn, 200)["data"]["users"], 0) ["points"] == 42
      assert Enum.at(json_response(conn, 200)["data"]["users"], 1) ["points"] == 43
    end

    test "get get no users", %{conn: conn} do
      ApiState.BaseMock
      |> expect(
        :view, fn() -> %ApiState.StateStruct{timestamp: ~U[2022-12-15 22:06:11.409030Z], min_number: 44} end
      )
      expected_response = %{
          "timestamp" => "2022-12-15T22:06:11.409030Z", 
          "users" => []
      }
      conn = get(conn, Routes.default_path(conn, :index))
      assert json_response(conn, 200)["data"] == expected_response
    end
  end

  defp create_users(_) do
    user1 = user_fixture(%{points: 40})
    user2 = user_fixture(%{points: 41})
    user3 = user_fixture(%{points: 42})
    user4 = user_fixture(%{points: 43})

    %{users: [user1, user2, user3, user4]}
  end
end